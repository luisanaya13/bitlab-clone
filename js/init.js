//Se inicia Jquery con ready
$(document).ready(function(){
	//Decimos que todo elemento html con clase .carrousel será un slick, el cual es eun slider y entre las lalve lleva la configuracion
	$('.carrousel').slick({
	  arrows:true, //Mostrar flrchas de navegacion
	  appendArrows: $('.navcarrousel'), //Que las flechas se agregen a ese elemento con esa clase, se puede usar un id si se quiere
	  infinite: true, //true indica que nuca termina, o sea, llega al final y no se detienen, vuelve a comenzar
	  speed: 300, //la velocidad de la transición en milisegundos
	  slidesToShow: 3, //Cuentos mostrar por vez
	  slidesToScroll: 1, //Cuantos se desplaza al hacer click en las flechas
	  responsive: [ //rea la version responsive, para que se adapte
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }]
	});
	//Creamos un nuevo slider, pero con configuraciones diferentes, este se usa en "Hire Talent"
	$('.slider').slick({
	  dots: true, //Muestra los dots o circulos para navegar
	  infinite: true, //Es infinito, no se detiene
	  speed: 300, //Velocidad de transicion
	  slidesToShow: 1, //Cuantos muestra en pantalla por vez
	  slidesToScroll: 1, //Cuantos se desplaza
	});
	
	//En becas al hacer click sobre los botones de aplicar a beca se abre un modal de pantalla completa. es acá que se crea el evento, todo boton con la clase .mostrarmodal abrira el mismo modal
	$('.mostrarmodal').click(function(){
		//Le decimos al modal que se muestre
		$('#solicitarbeca').modal('show');
	});
	
	//En el formulario de becas, hacemos validacion de cada input o select, para que no esten vacios, el evento blur es al perder el foco, o sea, cuando te salis de un input hacia otro lado
	$('#formbeca input, #formbeca select').blur(function(){
		//this es como decir "este" el que activó el evento
		var element = $(this);
		
		//Si el input que perdio foco está vacio
		if(element.val() == ''){
			//este busca el  elemento padre (el div que lo contiene) y busca el el elemento que tiene el mensaje de eror (.invalid-feedback) y aplica css para mostrarlo
			element.parent().find('.invalid-feedback').css({'display':'block'});
			element.css({'border-color':'red'});
		} else {
			//Si no está vacío, busca el elemento que contiene el mensaje de error y con css lo oculta
			element.parent().find('.invalid-feedback').css({'display':'none'});
			element.css({'border-color':'#ced4da'});
		}
	});

	$('#formdonar input').blur(function(){
		var element = $(this);

		if(element.val() == ''){
			element.parent().find('.invalid-feedback').css({'display':'block'});
			element.css({'border-color':'red'});
		} else {
			element.parent().find('.invalid-feedback').css({'display':'none'});
			element.css({'border-color':'#4e05a9'});
		}
	});
	
	//En donar se usa para la parte de "selecciona monto" que se vea bonito con cambios de clase cuando se selecciona un boton de donar una vez o mensual
	$(".donaciones button").on("click", function(){
	    var element = $(this).removeClass('btn-default').addClass('btn-morado').siblings().removeClass('btn-morado');
	});
	
	//Tomamos el js de bootstrap y le decimos que #MyTab será una tab
	$('#myTab a').click(function(){
		$(this).tab('show');
	})
});